package uk.co.gencoreoperative.ocelot.cli;

import com.martiansoftware.jsap.JSAPResult;
import com.martiansoftware.jsap.Parameter;
import com.martiansoftware.jsap.Switch;
import uk.co.gencoreoperative.fileutils.FileUtils;
import uk.co.gencoreoperative.ocelot.Ocelot;
import uk.co.gencoreoperative.ocelot.OcelotConstants;
import uk.co.gencoreoperative.ocelot.config.OcelotConfig;
import uk.co.gencoreoperative.ocelot.utils.FolderSearch;
import uk.co.gencoreoperative.ocelot.utils.Tomcat;

import javax.inject.Inject;
import java.io.File;
import java.text.MessageFormat;

/**
 * Responsible for finding an appropriate war in the local folder or
 * below, and then deploying this to the server and restarting the
 * server.
 *
 * @author robert.wapshott@forgerock.com
 */
public class Deploy implements Mode {
    // Injected
    private final Tomcat tomcat;
    private final OcelotConfig config;
    private final FileUtils fileUtils;
    private final FolderSearch folderSearch;

    @Inject
    public Deploy(Tomcat tomcat, OcelotConfig config, FileUtils fileUtils, FolderSearch folderSearch) {
        this.tomcat = tomcat;
        this.config = config;
        this.fileUtils = fileUtils;
        this.folderSearch = folderSearch;
    }

    @Override
    public Parameter getParameter() {
        return new Switch(OcelotConstants.DEPLOY_MODE)
                .setShortFlag('d')
                .setLongFlag(OcelotConstants.DEPLOY_MODE)
                .setHelp(MessageFormat.format(
                        "Searches for the war named \"{0}\" in the current working folder. If " +
                        "found, then will stop Tomcat, redploy the war and restart Tomcat.\n\n" +
                        "Note: This differs from \"{1}\" mode in that only the local folder " +
                        "is searched, rather than the defined \"{2}\" folder.",
                        config.getWarName(),
                        OcelotConstants.WAR_MODE,
                        config.getDevelopmentFolder()));
    }

    @Override
    public boolean canPerform(JSAPResult result) {
        return result.getBoolean(getParameter().getID());
    }

    @Override
    public void perform(JSAPResult result) {
        // Find the war file.
        File cwd = new File(System.getProperty("user.dir"));
        Ocelot.log("Searching for deployable " + config.getWarName() + " in " + cwd.getPath());
        File war = folderSearch.findWar(cwd);

        if (war == null) {
            Ocelot.exit("War file not located");
        }

        deployWar(tomcat, config, fileUtils, war);
    }

    static void deployWar(Tomcat tomcat, OcelotConfig config, FileUtils fileUtils, File war) {
        // Shutdown Tomcat
        tomcat.kill();

        Ocelot.log("Cleaning " + config.getWebAppName() + "...");
        tomcat.deleteWebApplication();

        Ocelot.log("Copying " + config.getWarName() + "...");
        // Copy war to the webapps folder.
        fileUtils.copy(war, config.getWebAppDeployedWar());


        Ocelot.log("Starting Tomcat...");
        // Start tomcat
        tomcat.start();
    }
}
