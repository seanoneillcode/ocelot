package uk.co.gencoreoperative.ocelot.tasks;

import com.google.inject.Inject;
import uk.co.gencoreoperative.fileutils.SearchUtils;
import uk.co.gencoreoperative.ocelot.Ocelot;
import uk.co.gencoreoperative.ocelot.utils.Filters;
import uk.co.gencoreoperative.ocelot.utils.Stats;
import uk.co.gencoreoperative.ocelot.config.OcelotConfig;
import uk.co.gencoreoperative.ocelot.utils.FilteredSearchAction;

import java.io.File;
import java.util.concurrent.ExecutorService;

/**
 * Responsible for scanning for class files.
 *
 * @author Robert Wapshott
 */
public class FolderScanner implements Runnable {
    private static final String CLASSES = "classes" + File.separator;
    private final OcelotConfig config;
    private final ExecutorService service;
    private final Coordinator coordinator;
    private final Stats stats;

    @Inject
    public FolderScanner(OcelotConfig config, ExecutorService service, Coordinator coordinator, Stats stats) {
        this.config = config;
        this.service = service;
        this.coordinator = coordinator;
        this.stats = stats;
    }

    @Override
    public void run() {
        SearchUtils.Action action = new SearchUtils.Action() {
            @Override
            public void perform(File source) {
                if (source.isDirectory()) throw new IllegalStateException();
                stats.incrementFilesChecked();

                File target = generateRelativeTarget(source);

                File parent = target.getParentFile();
                if (!parent.exists() && !parent.mkdirs()) {
                    throw new IllegalStateException("Couldn't create folder " + parent.getPath());
                }

                coordinator.fileToBeProcssed(target);
                FileCopier copier = Ocelot.getInstance(FileCopier.class);
                copier.setSource(source);
                copier.setTarget(target);
                service.execute(copier);
            }
        };
        SearchUtils.iterateTopDown(config.getDevelopmentFolder(), new FilteredSearchAction(config, action, new Filters()));

        // Signal that we have finished scanning.
        coordinator.setScanning(false);
    }

    private File generateRelativeTarget(File source) {
        String path = source.getPath();
        int pos = path.indexOf(CLASSES);
        String relative = path.substring(pos + FolderScanner.CLASSES.length(), path.length());
        return new File(config.getClassTargetFolder() + File.separator + relative);
    }
}
