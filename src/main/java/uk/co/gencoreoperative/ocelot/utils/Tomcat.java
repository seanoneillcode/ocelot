package uk.co.gencoreoperative.ocelot.utils;

import com.google.inject.Inject;
import uk.co.gencoreoperative.fileutils.FileUtils;
import uk.co.gencoreoperative.fileutils.StreamUtils;
import uk.co.gencoreoperative.ocelot.Ocelot;
import uk.co.gencoreoperative.ocelot.config.OcelotConfig;
import uk.co.gencoreoperative.process.Execute;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Responsible for providing functions to manipulate Tomcat.
 *
 * @author Robert Wapshott
 */
public class Tomcat {
    // Injected
    private final OcelotConfig config;
    private final FileUtils fileUtils;

    private static final String STARTED = "INFO: Server startup in";

    @Inject
    public Tomcat(OcelotConfig config, FileUtils fileUtils) {
        this.config = config;
        this.fileUtils = fileUtils;
    }

    /**
     * Find and kill an instance of Tomcat running on the system.
     *
     * Note: Makes a system call to both ps and kill.
     *
     * @return The PID of the tomcat instance that was killed.
     */
    public String kill() {
        List<String> pids = Execute.shell("ps -eaf | grep java | grep tomcat | grep -v grep | cut -c7-14");

        String pid = null;
        if (!pids.isEmpty()) {
            pid = pids.get(0).trim();
            Execute.shell("kill -9 " + pid);
            Ocelot.log("Shutdown Tomcat at " + pid);
        }
        return pid;
    }

    /**
     * Start Tomcat based on the tomcat folder provided in configuration.
     *
     * Note: This call blocks until Tomcat has started.
     */
    public void start() {

        String catalinaShell = new FileUtils().assemblePathFromParts(new String[]{
                config.getApacheTomcatHome().getPath(),
                "bin",
                "catalina.sh"});
        String catalinaLog = new FileUtils().assemblePathFromParts(new String[]{
                config.getApacheTomcatHome().getPath(),
                "logs",
                "catalina.out"});

        // Empty the catalina log file
        Execute.shell("> " + catalinaLog);

        // Start Tomcat
        Execute.command(catalinaShell, Arrays.asList("jpda", "start"));

        // Monitor the log file
        boolean start = false;
        List<String> output = new ArrayList<String>();
        while (!start) {
            // Read the log file
            List<String> lines = readFile(catalinaLog);

            // Check if the lines contain the signal
            for (int ii = 0; ii < lines.size(); ii++) {
                if (output.size() > ii) continue;
                String line = lines.get(ii);
                output.add(line);
                // Print new lines
                Ocelot.log(line);

                if (line.startsWith(STARTED)) {
                    start = true;
                    continue;
                }
            }

            // Finally sleep
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                throw new IllegalStateException(e);
            }
        }
    }

    /**
     * Deletes the web application from the Tomcat webapps folder.
     */
    public void deleteWebApplication() {
        // Clean up the previous deployment.
        File deployedFolder = config.getWebAppDeloyedFolder();
        File deployedWar = config.getWebAppDeployedWar();

        if (deployedFolder.exists()) fileUtils.removeFolder(deployedFolder);
        if (deployedWar.exists()) deployedWar.delete();
    }

    private List<String> readFile(String catalinaLog) {
        try {
            return StreamUtils.readLinesToList(new FileInputStream(catalinaLog));
        } catch (FileNotFoundException e) {
            throw new IllegalStateException(e);
        }
    }
}
