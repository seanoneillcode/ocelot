/**
 * Copyright 2013 ForgeRock, Inc.
 *
 * The contents of this file are subject to the terms of the Common Development and
 * Distribution License (the License). You may not use this file except in compliance with the
 * License.
 *
 * You can obtain a copy of the License at legal/CDDLv1.0.txt. See the License for the
 * specific language governing permission and limitations under the License.
 *
 * When distributing Covered Software, include this CDDL Header Notice in each file and include
 * the License file at legal/CDDLv1.0.txt. If applicable, add the following below the CDDL
 * Header, with the fields enclosed by brackets [] replaced by your own identifying
 * information: "Portions copyright [year] [name of copyright owner]".
 */
package uk.co.gencoreoperative.ocelot.utils;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * @author robert.wapshott@forgerock.com
 */
public class FiltersTest {
    @Test
    public void shouldAllowPath() {
        assertTrue(new Filters().filterMatch("badger", "weasel/badger/ferret"));
    }

    @Test
    public void shouldAllowWildcardsInFilter() {
        assertTrue(new Filters().filterMatch("weasel*ferret", "weasel/badger/ferret"));
    }

    @Test
    public void shouldAllowWildcardsAtStartAndEnd() {
        assertTrue(new Filters().filterMatch("*bad*", "weasel/badger/ferret"));
    }

    @Test
    public void shouldNotMatch() {
        assertFalse(new Filters().filterMatch("*eee*", "weasel/badger/ferret"));
    }
}
