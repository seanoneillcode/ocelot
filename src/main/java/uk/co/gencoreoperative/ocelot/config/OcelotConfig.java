package uk.co.gencoreoperative.ocelot.config;

import uk.co.gencoreoperative.fileutils.FileUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Responsible for storing the configuration for the application.
 *
 * @author Robert Wapshott
 */
public class OcelotConfig {
    private static final String WEBAPPS = "webapps";
    private static final String WAR_SUFFIX = ".war";
    private int version = 1;

    private File apacheTomcatHome;
    private String webApp;
    private File developmentFolder;
    private Collection<String> classFilters = new ArrayList<String>();
    private String warName;

    public File getApacheTomcatHome() {
        return apacheTomcatHome;
    }

    public void setApacheTomcatHome(File folder) {
        this.apacheTomcatHome = folder;
    }

    public String getWebAppName() {
        return webApp;
    }

    public void setWebAppName(String webAppName) {
        this.webApp = webAppName;
    }

    public File getDevelopmentFolder() {
        return developmentFolder;
    }

    public void setDevelopmentFolder(File folder) {
        this.developmentFolder = folder;
    }

    public Collection<String> getClassFilters() {
        return classFilters;
    }

    public void setClassFilters(Collection<String> filters) {
        this.classFilters = filters;
    }

    public String getWarName() {
        if (warName.endsWith(WAR_SUFFIX)) {
            return warName;
        }
        return warName + WAR_SUFFIX;
    }

    public void setWarName(String warName) {
        this.warName = warName;
    }

    public File getClassTargetFolder() {
        return new File(new FileUtils().assemblePathFromParts(new String[]{
                getApacheTomcatHome().getPath(),
                WEBAPPS,
                getWebAppName(),
                "WEB-INF",
                "classes"
        }));
    }

    /**
     * Identifies the version of this configuration when it comes handling previous versions.
     *
     * @return Positive int.
     */
    public int getVersion() {
        return version;
    }

    /**
     * @return The folder of the deployed web application.
     */
    public File getWebAppDeloyedFolder() {
        return new File(getWebAppsFolder(), getWebAppName());
    }

    /**
     * @return The deployed web application war.
     */
    public File getWebAppDeployedWar() {
        return new File(getWebAppsFolder(), getWebAppName() + WAR_SUFFIX);
    }

    /**
     * @return The Tomcat webapps folder where wars are deployed to.
     */
    public File getWebAppsFolder() {
        return new File(getApacheTomcatHome(), WEBAPPS);
    }
}
