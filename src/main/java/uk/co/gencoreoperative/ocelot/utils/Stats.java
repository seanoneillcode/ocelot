package uk.co.gencoreoperative.ocelot.utils;

import java.text.MessageFormat;

/**
 * Responsible for tracking the number of files and folders processed by the utility.
 *
 * @author Robert Wapshott
 */
public class Stats {
    private long filesChecked;
    private long filesCopied;

    public void incrementFilesCopied() {
        filesCopied++;
    }

    public void incrementFilesChecked() {
        filesChecked++;
    }

    public long getFilesCopied() {
        return filesCopied;
    }

    @Override
    public String toString() {
        return MessageFormat.format(
                "Files Scanned: {0}\n" +
                " Files Copied: {1}",
                filesChecked,
                filesCopied
        );
    }
}
