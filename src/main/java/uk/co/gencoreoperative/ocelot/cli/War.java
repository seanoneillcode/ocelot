package uk.co.gencoreoperative.ocelot.cli;

import com.google.inject.Inject;
import com.martiansoftware.jsap.JSAPResult;
import com.martiansoftware.jsap.Parameter;
import com.martiansoftware.jsap.Switch;
import uk.co.gencoreoperative.fileutils.FileUtils;
import uk.co.gencoreoperative.ocelot.Ocelot;
import uk.co.gencoreoperative.ocelot.OcelotConstants;
import uk.co.gencoreoperative.ocelot.config.OcelotConfig;
import uk.co.gencoreoperative.ocelot.utils.FolderSearch;
import uk.co.gencoreoperative.ocelot.utils.Tomcat;

import java.io.File;
import java.text.MessageFormat;

/**
 * Responsible for performing the somewhat lengthy operations of:
 *
 * - Stopping Tomcat
 * - Clean out Web Apps folder for the named application
 * - Find the war, and copy it in
 * - Start Tomcat.
 *
 * @author Robert Wapshott
 */
public class War implements Mode {
    // Injected
    private final Tomcat tomcat;
    private final OcelotConfig config;
    private final FileUtils fileUtils;
    private final FolderSearch folderSearch;

    @Inject
    public War(Tomcat tomcat, OcelotConfig config, FileUtils fileUtils, FolderSearch folderSearch) {
        this.tomcat = tomcat;
        this.config = config;
        this.fileUtils = fileUtils;
        this.folderSearch = folderSearch;
    }

    @Override
    public Parameter getParameter() {
        return new Switch(OcelotConstants.WAR_MODE)
                .setShortFlag('w')
                .setLongFlag(OcelotConstants.WAR_MODE)
                .setHelp(MessageFormat.format(
                        "Stops Tomcat and cleans out the web application \"{0}\". Then locates " +
                        "the war file \"{1}\" and copies it to \"{2}\" before restarting Tomcat.",
                        config.getWebAppName(),
                        config.getWarName(),
                        config.getWebAppDeployedWar().getPath()));
    }

    @Override
    public boolean canPerform(JSAPResult result) {
        return result.getBoolean(getParameter().getID());
    }

    @Override
    public void perform(JSAPResult result) {
        // Shutdown Tomcat
        tomcat.kill();

        Ocelot.log("Cleaning " + config.getWebAppName() + "...");
        tomcat.deleteWebApplication();

        // Find the war file.
        Ocelot.log("Searching for deployable " + config.getWarName() + "...");
        File folder = config.getDevelopmentFolder();
        File war = folderSearch.findWar(folder);
        if (war == null) {
            Ocelot.exit("Could not find " + config.getWarName());
        }

        Ocelot.log("Copying " + config.getWarName() + "...");
        // Copy war to the webapps folder.
        fileUtils.copy(war, config.getWebAppDeployedWar());


        Ocelot.log("Starting Tomcat...");
        // Start tomcat
        tomcat.start();
    }
}
