package uk.co.gencoreoperative.ocelot.cli;

import com.martiansoftware.jsap.FlaggedOption;
import com.martiansoftware.jsap.JSAPResult;
import com.martiansoftware.jsap.Parameter;
import uk.co.gencoreoperative.fileutils.FileUtils;
import uk.co.gencoreoperative.ocelot.Ocelot;
import uk.co.gencoreoperative.ocelot.OcelotConstants;
import uk.co.gencoreoperative.ocelot.config.OcelotConfig;
import uk.co.gencoreoperative.ocelot.utils.FolderSearch;
import uk.co.gencoreoperative.ocelot.utils.Tomcat;

import javax.inject.Inject;

import java.io.File;

/**
 * An extension of Deploy which will allow the caller to specify a specific file to deploy.
 */
public class DeployThis implements Mode {
    private final Tomcat tomcat;
    private final OcelotConfig config;
    private final FileUtils fileUtils;
    private final FolderSearch folderSearch;
    private String path;

    @Inject
    public DeployThis(Tomcat tomcat, OcelotConfig config, FileUtils fileUtils, FolderSearch folderSearch) {
        this.tomcat = tomcat;
        this.config = config;
        this.fileUtils = fileUtils;
        this.folderSearch = folderSearch;
    }

    @Override
    public Parameter getParameter() {
        return new FlaggedOption(OcelotConstants.DEPLOY_THIS_MODE)
                .setShortFlag('D')
                .setAllowMultipleDeclarations(false)
                .setLongFlag(OcelotConstants.DEPLOY_THIS_MODE)
                .setHelp("Takes the given war file and deploys it to Tomcat.");
    }

    @Override
    public boolean canPerform(JSAPResult result) {
        return result.getString(OcelotConstants.DEPLOY_THIS_MODE) != null;
    }

    @Override
    public void perform(JSAPResult result) {
        path = result.getString(OcelotConstants.DEPLOY_THIS_MODE);

        if (path == null || path.length() == 0) {
            Ocelot.exit("Failed to located war");
        }

        File war = new File(path);
        Deploy.deployWar(tomcat, config, fileUtils, war);
    }
}
